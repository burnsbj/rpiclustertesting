
#!/bin/bash

# 4 character tests

# tung tests
############

##########
# 1 Node #
##########
#tung n=2 t=1
mpirun -n 2 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 1 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 1 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 1 >>md5_test.csv

#tung n=2 t=2
mpirun -n 2 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 2 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 2 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 2 >>md5_test.csv

#tung n=2 t=3
mpirun -n 2 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 3 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 3 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 3 >>md5_test.csv

#tung n=2 t=4
mpirun -n 2 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 4 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 4 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 4 >>md5_test.csv

###########
# 2 Nodes #
###########

#tung n=3 t=1
mpirun -n 3 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 1 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 1 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 1 >>md5_test.csv

#tung n=3 t=2
mpirun -n 3 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 2 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 2 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 2 >>md5_test.csv

#tung n=3 t=3
mpirun -n 3 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 3 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 3 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 3 >>md5_test.csv

#tung n=3 t=4
mpirun -n 3 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 4 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 4 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 4 >>md5_test.csv

###########
# 3 Nodes #
###########

#tung n=4 t=1
mpirun -n 4 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 1 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 1 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 1 >>md5_test.csv

#tung n=4 t=2
mpirun -n 4 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 2 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 2 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 2 >>md5_test.csv

#tung n=4 t=3
mpirun -n 4 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 3 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 3 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 3 >>md5_test.csv

#tung n=4 t=4
mpirun -n 4 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 4 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 4 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid bb7d4b236b564cf1ec27aa891331e0af 4 4 >>md5_test.csv

echo 'tung test complete'

# bird tests
############


#!/bin/bash

##########
# 1 Node #
##########
#bird n=2 t=1
mpirun -n 2 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 1 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 1 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 1 >>md5_test.csv

#bird n=2 t=2
mpirun -n 2 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 2 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 2 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 2 >>md5_test.csv

#bird n=2 t=3
mpirun -n 2 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 3 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 3 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 3 >>md5_test.csv

#bird n=2 t=4
mpirun -n 2 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 4 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 4 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 4 >>md5_test.csv

###########
# 2 Nodes #
###########

#bird n=3 t=1
mpirun -n 3 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 1 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 1 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 1 >>md5_test.csv

#bird n=3 t=2
mpirun -n 3 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 2 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 2 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 2 >>md5_test.csv

#bird n=3 t=3
mpirun -n 3 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 3 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 3 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 3 >>md5_test.csv

#bird n=3 t=4
mpirun -n 3 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 4 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 4 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 4 >>md5_test.csv

###########
# 3 Nodes #
###########

#bird n=4 t=1
mpirun -n 4 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 1 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 1 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 1 >>md5_test.csv

#bird n=4 t=2
mpirun -n 4 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 2 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 2 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 2 >>md5_test.csv

#bird n=4 t=3
mpirun -n 4 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 3 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 3 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 3 >>md5_test.csv

#bird n=4 t=4
mpirun -n 4 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 4 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 4 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid abaecf8ca3f98dc13eeecbac263cd3ed 4 4 >>md5_test.csv

echo 'bird test complete'

# 5 character tests
###################

# mandy tests
#############

##########
# 1 Node #
##########
#mandy n=2 t=1
mpirun -n 2 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 1 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 1 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 1 >>md5_test.csv

#mandy n=2 t=2
mpirun -n 2 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 2 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 2 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 2 >>md5_test.csv

#mandy n=2 t=3
mpirun -n 2 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 3 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 3 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 3 >>md5_test.csv

#mandy n=2 t=4
mpirun -n 2 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 4 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 4 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 4 >>md5_test.csv

###########
# 2 Nodes #
###########

#mandy n=3 t=1
mpirun -n 3 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 1 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 1 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 1 >>md5_test.csv

#mandy n=3 t=2
mpirun -n 3 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 2 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 2 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 2 >>md5_test.csv

#mandy n=3 t=3
mpirun -n 3 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 3 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 3 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 3 >>md5_test.csv

#mandy n=3 t=4
mpirun -n 3 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 4 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 4 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 4 >>md5_test.csv

###########
# 3 Nodes #
###########

#mandy n=4 t=1
mpirun -n 4 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 1 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 1 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 1 >>md5_test.csv

#mandy n=4 t=2
mpirun -n 4 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 2 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 2 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 2 >>md5_test.csv

#mandy n=4 t=3
mpirun -n 4 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 3 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 3 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 3 >>md5_test.csv

#mandy n=4 t=4
mpirun -n 4 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 4 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 4 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 006ce4922072b1e7f8b733347fe1a40b 5 4 >>md5_test.csv

echo 'mandy test complete'

# burns test

##########
# 1 Node #
##########
#burns n=2 t=1
mpirun -n 2 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 1 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 1 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 1 >>md5_test.csv

#burns n=2 t=2
mpirun -n 2 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 2 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 2 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 2 >>md5_test.csv

#burns n=2 t=3
mpirun -n 2 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 3 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 3 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 3 >>md5_test.csv

#burns n=2 t=4
mpirun -n 2 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 4 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 4 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 4 >>md5_test.csv

###########
# 2 Nodes #
###########

#burns n=3 t=1
mpirun -n 3 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 1 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 1 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 1 >>md5_test.csv

#burns n=3 t=2
mpirun -n 3 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 2 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 2 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 2 >>md5_test.csv

#burns n=3 t=3
mpirun -n 3 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 3 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 3 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 3 >>md5_test.csv

#burns n=3 t=4
mpirun -n 3 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 4 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 4 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 4 >>md5_test.csv

###########
# 3 Nodes #
###########

#burns n=4 t=1
mpirun -n 4 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 1 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 1 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 1 >>md5_test.csv

#burns n=4 t=2
mpirun -n 4 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 2 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 2 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 2 >>md5_test.csv

#burns n=4 t=3
mpirun -n 4 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 3 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 3 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 3 >>md5_test.csv

#burns n=4 t=4
mpirun -n 4 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 4 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 4 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 46defce884d1be32019f20864172323d 5 4 >>md5_test.csv

echo 'burns test complete'

# 6 character tests
###################

# amanda tests
##############

##########
# 1 Node #
##########
#amanda n=2 t=1
mpirun -n 2 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 1 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 1 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 1 >>md5_test.csv

#amanda n=2 t=2
mpirun -n 2 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 2 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 2 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 2 >>md5_test.csv

#amanda n=2 t=3
mpirun -n 2 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 3 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 3 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 3 >>md5_test.csv

#amanda n=2 t=4
mpirun -n 2 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 4 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 4 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 4 >>md5_test.csv

###########
# 2 Nodes #
###########

#amanda n=3 t=1
mpirun -n 3 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 1 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 1 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 1 >>md5_test.csv

#amanda n=3 t=2
mpirun -n 3 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 2 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 2 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 2 >>md5_test.csv

#amanda n=3 t=3
mpirun -n 3 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 3 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 3 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 3 >>md5_test.csv

#amanda n=3 t=4
mpirun -n 3 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 4 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 4 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 4 >>md5_test.csv

###########
# 3 Nodes #
###########

#amanda n=4 t=1
mpirun -n 4 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 1 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 1 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 1 >>md5_test.csv

#amanda n=4 t=2
mpirun -n 4 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 2 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 2 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 2 >>md5_test.csv

#amanda n=4 t=3
mpirun -n 4 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 3 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 3 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 3 >>md5_test.csv

#amanda n=4 t=4
mpirun -n 4 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 4 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 4 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 6209804952225ab3d14348307b5a4a27 6 4 >>md5_test.csv

echo 'amanda test complete'

# openmp
########


#!/bin/bash

##########
# 1 Node #
##########
#openmp n=2 t=1
mpirun -n 2 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 1 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 1 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 1 >>md5_test.csv

#openmp n=2 t=2
mpirun -n 2 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 2 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 2 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 2 >>md5_test.csv

#openmp n=2 t=3
mpirun -n 2 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 3 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 3 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 3 >>md5_test.csv

#openmp n=2 t=4
mpirun -n 2 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 4 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 4 >>md5_test.csv
mpirun -n 2 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 4 >>md5_test.csv

###########
# 2 Nodes #
###########

#openmp n=3 t=1
mpirun -n 3 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 1 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 1 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 1 >>md5_test.csv

#openmp n=3 t=2
mpirun -n 3 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 2 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 2 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 2 >>md5_test.csv

#openmp n=3 t=3
mpirun -n 3 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 3 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 3 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 3 >>md5_test.csv

#openmp n=3 t=4
mpirun -n 3 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 4 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 4 >>md5_test.csv
mpirun -n 3 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 4 >>md5_test.csv

###########
# 3 Nodes #
###########

#openmp n=4 t=1
mpirun -n 4 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 1 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 1 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 1 >>md5_test.csv

#openmp n=4 t=2
mpirun -n 4 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 2 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 2 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 2 >>md5_test.csv

#openmp n=4 t=3
mpirun -n 4 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 3 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 3 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 3 >>md5_test.csv

#openmp n=4 t=4
mpirun -n 4 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 4 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 4 >>md5_test.csv
mpirun -n 4 --hostfile hosts ./md5_hybrid 1dcf8ca59710cf3121728598da7a3f2f 6 4 >>md5_test.csv

echo 'openmp test complete'

