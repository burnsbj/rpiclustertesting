COMP = mpic++

FLAGS = -DOPENSSL

LIBS += -lcrypto
LIBS += -fopenmp

EXECS = md5_hybrid

#all:
#	md5_hybrid	copyto

copyto: md5_hybrid
	sh copyto_fs.sh

md5_hybrid:
	$(COMP) $(FLAGS) md5_hybrid.cpp -o md5_hybrid $(LIBS)


.PHONY:clean

clean:
	rm -f $(EXECS) *.o


