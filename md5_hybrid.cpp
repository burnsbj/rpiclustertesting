#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <openssl/md5.h>
#include <math.h>
#include <mpi.h>
#include <time.h>
#include <unistd.h>
#include <omp.h>

#define LENCH 26
#define NE 20
#define RESULT 60
using namespace std;

char character[LENCH] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'
    , 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

// get index of character in distionary
int get_index(char ch){
	int i;
	for (i = 0; i< LENCH; i++){
		if (character[i] == ch){
			return i;
		}
	}
	return LENCH;
};

// convert pass in type number to string
void convert_pass_num2str(int pass, int len, char conv_pass[]){
	int i = len - 1;
	int j;
	int point;

	for (j =0 ; j < len ; j++){
		conv_pass[j] = 'a';
	}
	conv_pass[len] = '\0';

	while(1){
		if (pass >0){
			point = pass % LENCH;
			conv_pass[i] = character[point];
			i--;
			pass = pass / LENCH;
		}else{
			break;
		}
	}
};

// generate next password 
void gen_pass(int num_pass, int len,char conv_pass[]){
	char* new_pass = NULL;
	num_pass ++;
	convert_pass_num2str(num_pass, len,conv_pass);
};

// encode password to MD5 hash
void encode_password_MD5(const char *string, char *mdString){
    unsigned char digest[16];

    // encode password into MD5
    MD5_CTX ctx;
    MD5_Init(&ctx);
    MD5_Update(&ctx, string, strlen(string));
    MD5_Final(digest, &ctx);
    for (int i = 0; i < 16; i++)
        sprintf(&mdString[i*2], "%02x", (unsigned int)digest[i]);
};

// decryt password in a node
void decrypt_pass(int begin, int end, char *mdString, int len, int rank, int nThreads){
	char pas[len];
	char temp_mdString[33];
	bool found = false;
	int i, j, tid;

	#pragma omp parallel for shared(found, len, mdString, rank, nThreads) private(pas, temp_mdString, i, tid)
	for(i = begin; i < end; i++){
		tid = omp_get_thread_num();
		if(found)
			i = end;
		convert_pass_num2str(i, len, pas);
		encode_password_MD5(pas, temp_mdString);
		if(!strcmp(temp_mdString, mdString)){
			//printf("%s,%d,%d,", pas, tid, rank);
			MPI_Send(&pas, len, MPI_INT, 0, RESULT, MPI_COMM_WORLD);
			MPI_Send(&tid, 1, MPI_INT, 0, RESULT, MPI_COMM_WORLD);
			MPI_Send(&rank, 1, MPI_INT, 0, RESULT, MPI_COMM_WORLD);
			found = true;
		}
	}
	if (!found){
		char err_pass[len];
		err_pass[0] = '\0';
		MPI_Send(&err_pass, 1, MPI_INT, 0, RESULT, MPI_COMM_WORLD);
		MPI_Send(&tid, 1, MPI_INT, 0, RESULT, MPI_COMM_WORLD);
		MPI_Send(&rank, 1, MPI_INT, 0, RESULT, MPI_COMM_WORLD);
	}

	return;
};

// function with rank0 (frontend node), split data and send to other node and receive data response
void rank0(char *mdString, int number_process, int len_password){
	int begin, end, tid, rank;
	char pas[len_password + 1];
	int i;
	MPI_Status status;
    int total_password = pow(LENCH, len_password); //Total possible passwords PossibleChar^CharInPass
    int part = total_password / (number_process-1);//Possible Passwords per node

    for (i = 1; i< number_process; i++){
    	begin = part * (i-1);
    	if (i != number_process - 1){
    		end = begin + part -1;
			}else{
				end = total_password -1;
			}

			MPI_Send(&begin, 1, MPI_INT, i, NE, MPI_COMM_WORLD);
			MPI_Send(&end, 1, MPI_INT, i, NE, MPI_COMM_WORLD);
		}

	for (i = 1; i < number_process;i++){
		MPI_Recv(&pas, len_password, MPI_INT, i, RESULT, MPI_COMM_WORLD, &status);
		MPI_Recv(&tid, 1, MPI_INT, i, RESULT, MPI_COMM_WORLD, &status);
		MPI_Recv(&rank, 1, MPI_INT, i, RESULT, MPI_COMM_WORLD, &status);
		if (pas[0] != '\0'){
			printf("%s,%d,%d,", pas, rank, tid);
			break;
		}
	}
	return;
}

// compute node, decrypt to find pass, receive data by frontend node and send result
void ranki(char* mdString, int len_password, int rank, int nThreads){
	int begin;
	int end;
	int count;
	int tid;
	MPI_Status status;
	MPI_Recv(&begin, 1, MPI_INT, 0, NE, MPI_COMM_WORLD, &status);
	MPI_Recv(&end, 1, MPI_INT, 0, NE, MPI_COMM_WORLD, &status);

	decrypt_pass(begin, end, mdString, len_password, rank, nThreads);
}

int main(int argc, char* argv[])
{
	int size, rank, tid, nThreads;
	char hostname[50];
	MPI_Init(&argc, &argv);
    char mdString[33];
    int len_password;

    // get arg to variable
    if(argc != 4) return -1;
    sscanf(argv[1], "%s", mdString);
    sscanf(argv[2], "%d", &len_password);
		sscanf(argv[3], "%d", &nThreads);

    // get rank and size of MPI
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	omp_set_num_threads(nThreads);
	clock_t t1,t2;
    t1 = clock();
	if (rank == 0){
		// if node is frontend
		printf("%s,%d,%d,%d,", mdString, len_password, size, nThreads);
		rank0(mdString, size, len_password);
		t2=clock();
    	float diff ((float)t2-(float)t1);
    	float seconds = diff / CLOCKS_PER_SEC;
    	printf("%f\n", seconds);
	}else{
		// if node is compute
		ranki(mdString, len_password, rank, nThreads);
	}

	MPI_Finalize();

	return 0;
}

// mpic++ md5.cpp -o md5  -lcrypto
// mpirun -hostfile hostfile -np 2 ./md5 10b30316838d2c01e7cb6350178ba812 5




