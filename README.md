# Raspberry Pi 4 B 8GB RAM Cluster Testing #

A repository for holding code to test a 4 node cluster of Raspberry Pi 4 Model B as part of a graduate
class project.

md5 hash cracking code originally forked from [tungct on github](https://github.com/tungct/mpi-crack-md5). 
I would have just put it in Github but I can't get my Raspberry Pis to connect to github.com and that is not high
enough of a priority to delay writing the paper for school.

More info to come in Readme file. Writing the paper for school is higher priority.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact